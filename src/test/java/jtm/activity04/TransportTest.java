package jtm.activity04;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TransportTest {

    @Test
    public void getTypeTest(){
        Transport transport = new Transport("ID1",10.0f,60);
        assertEquals("ID1 Transport",transport.getType());

        transport.setId("ID2");
        assertEquals("ID2 Transport",transport.getType());
    }

    @Test
    public void toStringTest(){
        Transport transport = new Transport("ID1",10.0f,60);
        assertEquals("Id:ID1 cons:10.0l/100km, tank:60l, fuel:60.00l",transport.toString());
    }

    @Test
    public void moveEnoughFuelTest(){

        int distance = 240;
        float cons = 10.0f;
        int tankSize = 60;

        Transport transport = new Transport("ID1",cons,tankSize);
        Road road = new Road("Riga","Liepaja",distance);

        String res = transport.move(road);

        assertEquals("ID1 Transport is moving on Riga - Liepaja, 240km",res);

        assertEquals(tankSize - ((cons * distance) / 100) ,transport.getFuelInTank(), 0.1f);
    }

    @Test
    public void moveNotEnoughFuelTest(){
        int distance = 640;
        float cons = 10.0f;
        int tankSize = 60;

        Transport transport = new Transport("ID1",cons,tankSize);
        Road road = new Road("Riga","Liepaja",distance);

        String res = transport.move(road);
        assertEquals("Cannot move on Riga - Liepaja, 640km. Necessary fuel:64.00l, fuel in tank:60.00l",res);
        assertEquals(tankSize,transport.getFuelInTank(), 0.1f);
    }


}
