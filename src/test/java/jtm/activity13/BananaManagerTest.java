package jtm.activity13;

import org.junit.Before;
import org.junit.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BananaManagerTest {

    BananaManager bananaManager;
    Connection conn;
    ResultSet rs;
    PreparedStatement pStmt;

    @Before
    public void setUp() throws Exception {

        conn = mock(Connection.class);
        pStmt = mock(PreparedStatement.class);
        rs = mock(ResultSet.class);

        //Mocking method with return type
        when(conn.prepareStatement(anyString())).thenReturn(pStmt);
        when(pStmt.executeQuery()).thenReturn(rs);
        //Mocking void method
        doNothing().when(pStmt).setDouble(anyInt(), anyDouble());

        bananaManager = new BananaManager();
        bananaManager.conn = conn;
    }

    @Test
    public void findBananaByWeightMultipleResultsTest() throws Exception {

        //Mocking multiple method calls - on first 3 invocations true will be returned on fourth false
        when(rs.next()).thenReturn(true, true, true, false);
        when(rs.getDouble(1)).thenReturn(102.4, 103.2, 100.2);
        when(rs.getDouble(2)).thenReturn(24.4, 25.2, 23.2);
        when(rs.getString(3)).thenReturn("Brownish", "Yellow-Green", "Yellow");
        when(rs.getDate(4)).thenReturn(new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()), new Date(System.currentTimeMillis()));

        List<Banana> resList = bananaManager.findBananaByWeight(100);
        System.out.println(resList);
        assertEquals(3,resList.size());

    }


}
