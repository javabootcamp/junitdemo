package jtm.activity04;

import java.util.Locale;

public class Transport {
    // Do not change access modifiers to encapsulate internal properties!
    private String id; // Transport registration number
    private float consumption; // fuel consumption in litres per 100km
    private int tankSize; // tank size in litres
    private float fuelInTank; // fuel in tank


    public Transport(String id, float consumption, int tankSize) {
        super();
        this.id = id;
        this.consumption = consumption;
        this.tankSize = tankSize;
        this.fuelInTank = 1.0f * tankSize;
    }


    public String toString() {
        return "Id:" + this.id + " cons:" + String.format(Locale.US, "%.1f", this.consumption) +
                "l/100km, tank:" + String.format(Locale.US, "%02d", this.tankSize) + "l, fuel:" +
                String.format(Locale.US, "%.2f", this.fuelInTank) + "l";
    }


    protected final String getType() {
        return this.id + " " + this.getClass().getSimpleName();
    }


    public String move(Road road) {
        float necessaryFuel = (road.getDistance() * 1.0f / 100) * this.consumption;
        if (necessaryFuel < this.fuelInTank) {
            this.fuelInTank -= necessaryFuel;
            return this.getType() + " is moving on " + road.getFrom() + " - " + road.getTo() + ", " + road.getDistance() + "km";
        }
        return "Cannot move on " + road.getFrom() + " - " + road.getTo() + ", " + road.getDistance() +
                "km. Necessary fuel:" + String.format(Locale.US, "%.2f", necessaryFuel) + "l, fuel in tank:" +
                String.format(Locale.US, "%.2f", this.getFuelInTank()) + "l";
    }


    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }


    public float getConsumption() {
        return consumption;
    }


    public void setConsumption(float consumption) {
        this.consumption = consumption;
    }


    public int getTankSize() {
        return tankSize;
    }


    public void setTankSize(int tankSize) {
        this.tankSize = tankSize;
    }


    public float getFuelInTank() {
        return fuelInTank;
    }


    public void setFuelInTank(float fuelInTank) {
        this.fuelInTank = fuelInTank;
    }

}