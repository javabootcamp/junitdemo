package jtm.activity13;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BananaManager {

    protected Connection conn;

    public BananaManager() {
        if (conn == null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                conn = DriverManager.getConnection("jdbc:mysql://localhost/?autoReconnect=true&useSSL=false", "root",
                        "abcd1234");
                conn.setAutoCommit(false);
            } catch (Exception e) {
                System.err.println(e);
            }
        }
    }

    public List<Banana> findBananaByWeight(double weight) {

        List<Banana> list = new ArrayList<>();
        try {
            PreparedStatement pStmt = null;
            pStmt = conn.prepareStatement("SELECT weight, length, color, birthDate FROM dbo.Banana WHERE weight > ?");
            pStmt.setDouble(1, weight);

            ResultSet rs = pStmt.executeQuery();
            while (rs.next()) {
                list.add(new Banana(rs.getDouble(1), rs.getDouble(2), rs.getString(3), rs.getDate(4)));
            }

        } catch (SQLException e) {
            System.err.println(e);
        }
        return list;
    }


}