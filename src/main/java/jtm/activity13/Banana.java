package jtm.activity13;

import java.util.Date;

public class Banana {

    public double weight;
    public double length;
    public String color;
    public Date birth;

    public Banana(double weight, double length, String color, Date birth) {
        this.weight = weight;
        this.length = length;
        this.color = color;
        this.birth = birth;
    }

    public Banana(){

    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    @Override
    public String toString() {
        return "Banana{" +
                "weight=" + weight +
                ", length=" + length +
                ", color='" + color + '\'' +
                ", birth=" + birth +
                '}';
    }
}
